package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Runner {
    private static final Logger logger = LogManager.getLogger(Runner.class);
    public static void main(String[] args) {
        Random random = new Random();
        do {
            int n = random.nextInt(100);
            logger.trace("this is trace message. n = " + n);
            if (n % 2 == 0){
                logger.debug("this is debug message. n = " + n);
            }
            if (n % 3 == 0){
                logger.info("this is info message. n = " + n);
            }
            if (n % 5 == 0){
                logger.warn("this is warn message. n = " + n);
            }
            if (n % 7 == 0){
                try{
                    throw new RuntimeException();
                } catch (RuntimeException e){
                    logger.error("this is error message. n = " + n, e);
                }
            }
            if (n % 11 == 0){
                try {
                    throw new Exception();
                } catch (Exception e){
                    logger.fatal("this is fatal message. n = " + n, e);
                    return;
                }
            }
        } while (true);
    }
}
